import React from 'react';
import ReactDOM from 'react-dom/client';

import './index.css';
import App from './App';

// App 
const Application = (
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// get root element
const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

// render app
root.render(Application);