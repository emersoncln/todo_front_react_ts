import { TodoType } from "../../shared-types/types";
import API from "./api";

export const findAll = async () => {
  const response = await API.get(`todos`);
  return response.data;
};

export const deleteById = async (id: number) => {
    const response = await API.delete(`todos/${id}`);
    return response.data;
};

export const updateById = async (id: number, model: TodoType) => {
  const response = await API.put(`todos/${id}`, {
    ...model,
  });
  return response.data;
};

export const createTodo = async (model: TodoType) => {
  const response = await API.post(`todos`, {
    ...model,
  });
  return response.data;
};
