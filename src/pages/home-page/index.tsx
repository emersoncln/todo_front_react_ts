import { Box, Button, Checkbox, FormControl, FormControlLabel, InputLabel, Modal, Paper, TextField, Typography } from '@mui/material';
import { ChangeEvent, useCallback, useEffect, useMemo, useState } from 'react';
import { AddOutlined, ExitToAppOutlined } from '@mui/icons-material';

import './styles.css';
import TodoCard from '../../components/TodoCard';
import { TodoType } from '../../shared-types/types';
import { createTodo, deleteById, findAll, updateById } from '../../resources/api/functions';

export const App = () => {
    // states
    const [todo, setTodo] = useState<string>("");
    const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
    const [todosList, setTodosList] = useState<Array<TodoType>>([]);
    const [checked, setChecked] = useState<boolean>(false);
    const [inEdit, setInEdit] = useState<boolean>(false);
    const [selected, setSelected] = useState<TodoType>({ id: -1, name: "", checked: false });

    // handle todo changes
    const handleChange = useCallback((event: ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;
        if (name === "todo") {
            setTodo(value);
        }
    }, []);

    const handleClose = useCallback(() => {
        setIsModalOpen(false);
        setSelected({ id: -1, name: "", checked: false });
        setInEdit(false);
        setChecked(false);
        setTodo("");
    }, []);

    const listAllTodos = useCallback(async () => {
        try {
            const response = await findAll();
            setTodosList(response);
        } catch (error: any) {
            console.log(error.message);
        }
    }, []);

    useEffect(() => {
        listAllTodos()
    }, [listAllTodos]);

    const onCreateTodo = useCallback(async () => {
        const data = { id: -1, name: todo, checked: false };
        try {
            await createTodo(data);
            setTimeout(() => {
                handleClose();
                listAllTodos();
            }, 1000);
        } catch (error: any) {
            console.log(error.message);
        }
    }, [todo, listAllTodos, handleClose]);

    const onEditTodo = useCallback(async (id: number) => {
        const data = { id: id, name: todo, checked: checked };
        try {
            await updateById(id, data);
            setTimeout(() => {
                handleClose();
                listAllTodos();
            }, 1000);
        } catch (error: any) {
            console.log(error.message);
        }
    }, [todo, handleClose, checked, listAllTodos]);

    const onRemoveTodo = useCallback(async (id: number) => {
        try {
            await deleteById(id);
            setTimeout(() => {
                listAllTodos()
            }, 1000);
        } catch (error: any) {
            console.log(error.message);
        }
    }, [listAllTodos]);

    const verifyBeforeEdit = useCallback((item: TodoType) => {
        setSelected(item);
        setTodo(item.name);
        setChecked(item.checked);
        setIsModalOpen(true);
        setInEdit(true);
    }, []);

    // todo modal
    const renderAddModal = useMemo(() => {
        return (
            <Modal
                className="modal-add-todo"
                style={{ border: "none", borderColor: "#ffff", borderWidth: 0, zIndex: 2 }}
                open={isModalOpen}
                onClose={() => { }}
                disableAutoFocus={true}
                hideBackdrop={false}
                slotProps={{ backdrop: { hidden: false } }}
            >
                <Paper style={{
                    position: "absolute",
                    top: "50%",
                    left: "50%",
                    transform: "translate(-50%, -50%)",
                    background: "#f3f4f6",
                    border: "none",
                    borderRadius: 5,
                    color: "black",
                    justifyContent: "center",
                    alignItems: "center",
                    lineBreak: "anywhere",
                    width: 700,
                    padding: 10,
                }}>
                    <h4>Novo</h4>
                    <FormControl
                        className="form-group"
                        style={{ display: "flex", width: "100%" }}
                    >
                        <InputLabel id="multiple-todo-label" shrink>
                            To Do
                        </InputLabel>
                        <TextField
                            name="todo"
                            type="text"
                            className="form-control new-todo-control"
                            value={todo}
                            InputProps={{ label: "To Do", notched: true }}
                            placeholder="Digite sua tarefa."
                            onChange={(event: ChangeEvent<HTMLInputElement>) => {
                                handleChange(event);
                            }}
                        />
                    </FormControl>

                    {inEdit && <FormControlLabel
                        style={{ marginRight: 5, marginLeft: 5 }}
                        control={
                            <Checkbox
                                checked={checked}
                                onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                                    setChecked(event.target.checked)
                                }}
                            />
                        }
                        label="Marcar como concluído"
                    />}

                    <Box className={"footer"} style={{ marginTop: 10, display: "flex", justifyContent: "end" }}>
                        <Button onClick={() => handleClose()} className="icon-button add-todo" style={{ height: 40, width: 150, background: "Orange", color: "#ffff", marginRight: 10 }} >
                            <Typography color={"#ffff"} fontSize={16} style={{ marginRight: 5 }}>Cancelar</Typography><ExitToAppOutlined color={"inherit"} />
                        </Button>
                        <Button onClick={() => inEdit ? onEditTodo(selected?.id!!) : onCreateTodo()} className="icon-button add-todo" style={{ height: 40, width: 150, background: "cadetBlue", color: "#ffff" }} >
                            <Typography color={"#ffff"} fontSize={16} style={{ marginRight: 5 }}>Salvar</Typography><AddOutlined color={"inherit"} />
                        </Button>
                    </Box>
                </Paper>
            </Modal>
        )
    }, [todo, isModalOpen, onCreateTodo, handleChange, handleClose, selected, inEdit, onEditTodo, checked]);

    // app template
    const template = useMemo(() => {
        return (
            <Box className="App">
                <h5>Minhas Tarefas</h5>
                <Paper className="header-app" style={{ background: "#ffff", padding: 20, width: 650, height: 40, margin: 5, display: "flex", justifyContent: "end", alignItems: "end" }}>
                    <Box className={"footer"} style={{ marginTop: 10 }}>
                        <Button onClick={() => setIsModalOpen(true)} className="icon-button add-todo" style={{ height: 40, width: 150, background: "cadetBlue", color: "#ffff" }} >
                            <Typography color={"#ffff"} fontSize={16} style={{ marginRight: 5 }}>Novo</Typography><AddOutlined color={"inherit"} />
                        </Button>
                    </Box>
                </Paper>
                <Paper className={"content"} style={{ padding: 20, width: 650 }}>
                    <Box style={{ marginTop: 10 }}>
                        <TodoCard todoList={todosList} onRemoveTodo={onRemoveTodo} onEditTodo={verifyBeforeEdit} />
                    </Box>
                </Paper>
                {renderAddModal}
            </Box>
        );
    }, [renderAddModal, todosList, onRemoveTodo, verifyBeforeEdit]);

    return template;
}