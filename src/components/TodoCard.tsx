import { useMemo } from "react";

import { DeleteOutlineOutlined, EditOutlined } from "@mui/icons-material";
import { Box, Chip, Paper, Typography } from "@mui/material";
import { TodoType } from "../shared-types/types";

const TodoCard = ({ todoList, onRemoveTodo, onEditTodo }: { todoList: Array<TodoType>, onRemoveTodo: (...args: any) => void , onEditTodo: (...args: any) => void }) => {
    const template = useMemo(() => {
        return (
            <>
                {todoList.map((item, index) => {
                    return <Paper style={{ background: "#f5f5f5", borderRadius: 5, padding: 20, display: "flex", lineBreak: "anywhere", margin: 10 }} key={index}>
                        <Box style={{ width: "60%" }}>
                            <Typography style={{ marginTop: 10, textDecoration: item.checked ? "line-through" : "auto" }} >
                                {item.name}
                            </Typography>
                        </Box>
                        {item.checked ? <Box style={{ width: "40%" }}>
                            <Typography style={{ marginTop: 10 }} >
                                Concluído
                            </Typography>
                        </Box>
                            : <Box style={{ width: "40%", display: "flex" }}>
                                <Chip
                                    className="chip-edit"
                                    style={{ marginRight: 10 }}
                                    onClick={() => {
                                        onEditTodo(item)
                                    }}
                                    label="Editar"
                                    icon={
                                        <EditOutlined
                                            style={{ height: 24, width: 24 }}
                                        />
                                    }
                                />
                                <Chip
                                    className="chip-remove"
                                    onClick={() => {
                                        onRemoveTodo(item.id)
                                    }}
                                    label="Remover"
                                    icon={
                                        <DeleteOutlineOutlined
                                            style={{ height: 24, width: 24 }}
                                        />
                                    }
                                />
                            </Box>}
                    </Paper>
                })}
            </>
        );
    }, [todoList, onRemoveTodo, onEditTodo]);

    return template;
}

export default TodoCard;