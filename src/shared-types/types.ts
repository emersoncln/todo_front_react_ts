export type TodoType = {
    id?: number;
    name: string;
    checked: boolean;
};